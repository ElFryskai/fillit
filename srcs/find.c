/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: groussel <groussel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 03:52:17 by groussel          #+#    #+#             */
/*   Updated: 2018/04/09 09:18:59 by groussel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** TODO:	[ ] Start at the top left
**			[ ] If you can't put, x++
**			[ ] If space between first and second shape, x = 0 and y++
**			[ ] If still impossible, backtracking (reser letters, pass to next shape)
**			https://medium.com/@andreaiacono/backtracking-explained-7450d6ef9e1a
**			https://www.wikiwand.com/fr/Algorithme_de_remplissage_par_diffusion
**			[ ] If still impossible (shape reach border), add 1*1 to the map
**
** FUNC:	exit
**			open / close
**			malloc / free
**			write / reads
**
** FIXME:	[ ] Nothing yet
*/

#include "fillit.h"
#include "libft.h"

int		start(t_shapes *shapes)
{

}